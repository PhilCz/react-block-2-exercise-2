import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Names from "./components/Names";
import LastNames from "./components/LastNames";

class App extends React.Component {
  render() {
    let names = ["John ", "Arnold ", "Lionel ", "Jean", "Morgan "];
    let lastNames = ["Snow ", "Swarchzenegger ", "Messi ", "Reno ", "Freeman"];

    

    return (
      <div classname="App">
        <Names names = {names} />
        <LastNames lastNames ={lastNames} /> 
      </div>
    );
  }
}
export default App;
