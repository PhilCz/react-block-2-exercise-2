import React from 'react';

class LastNames extends React.Component {

    lastnamesloop = () => (
        this.props.lastNames.map((lastNames, i) => lastNames)
    )
    
    render(){
        return(
            <div>
              {this.lastnamesloop()} 
            </div>
        );
    }
}

export default LastNames;