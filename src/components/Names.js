import React from 'react';

class Names extends React.Component {

    firstnamesloops = () => (
        this.props.names.map((name, i) => name)
    )
    
    render(){
        return(
            <div>
                {this.firstnamesloops()}
            </div>
        );
    }
}

export default Names;